from picamera.array import PiRGBArray
from picamera import PiCamera
import requests
import json
import numpy as np
from utils.plot_target import plot_target

url='http://127.0.0.1:5000'

IM_WIDTH = 1280
IM_HEIGHT = 720

camera = PiCamera()
camera.resolution = (IM_WIDTH, IM_HEIGHT)
raw_capture = PiRGBArray(camera, size=(IM_WIDTH, IM_HEIGHT))
camera.capture(raw_capture, 'bgr')
a = raw_capture.array

shp = ', '.join([str(d) for d in a.shape])
files = {'file': a.tobytes(), 'shape': shp}

r = requests.post(url, files=files)
d = json.loads(r.text)
d = d['response']

if len(d) > 0:
    plot_target(d[0]['box'], a)


print(d)
