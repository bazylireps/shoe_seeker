import sys
from time import sleep
import numpy as np
from lidar.stepper_utils import step, calibrate, set_stepper_mode, is_on_spot, change_direction, set_clockwise, \
    set_counterclockwise, turn_stepper_on
from lidar.lidar_utils import get_distance
from drive.motor_utils import step_backwards, step_forward, step_left, step_right, drive_setup
from utils.plot_target import plot_target
from picamera.array import PiRGBArray
from picamera import PiCamera
import requests
import json

url = 'http://127.0.0.1:5000'

IM_WIDTH = 640
IM_HEIGHT = 480

camera = PiCamera()
camera.resolution = (IM_WIDTH, IM_HEIGHT)
raw_capture = PiRGBArray(camera, size=(IM_WIDTH, IM_HEIGHT))

delay2 = 0.003

target = 'bicycle'


def turn(mode, delay1):
    found_object = False
    missed_steppes_scan = 0
    missed_steppes_return = 0
    set_stepper_mode(mode)
    direction = set_counterclockwise()

    steps = 200
    steps *= mode

    sleep(0.1)
    print("skanuje w trybie 1/%d" % (mode))
    sleep(0.1)
    points = list()
    for camera_pos in range(steps):
        print(f'camera pos: {camera_pos}')
        step(delay1, 0)
        dist, strength = get_distance()
        points.append((camera_pos, dist, strength))
        if camera_pos % (20 * mode) == 0:
            new_camera_pos = center_image(camera_pos, steps)
            diff = camera_pos - new_camera_pos
            if diff == 0:
                continue
            camera_pos = new_camera_pos
            print(f'new camera pos: {camera_pos}')
            set_clockwise()
            step_func = step_left
            while abs(camera_pos) > 4:
                drive_steps = int(abs(camera_pos) / steps * 73)
                for drive_step in range(drive_steps):
                    step_func()
                    for i in range(5):
                        step(delay2, 0)
                calibrate(delay2, 0, mode)
                camera_pos = center_image(0, steps)
                if camera_pos < 0:
                    set_counterclockwise()
                    step_func = step_right
                else:
                    set_clockwise()
                    step_func = step_left

            return

    if not is_on_spot():
        missed_steppes_scan = calibrate(delay1, delay2, mode)
        print("zgubiono %d krokow podczas skanu!" % missed_steppes_scan)
    change_direction(direction)
    for x in range(steps):
        step(delay2, 0)

    sleep(0.5)
    if not is_on_spot():
        missed_steppes_return = calibrate(delay1, delay2, mode)
        print("zgubiono %d krokow podczas powrotu!" % (missed_steppes_return))

    return points, missed_steppes_scan, missed_steppes_return


target_images = []
current_img = None


def center_image(camera_pos, steps):
    while True:
        img_data = scan_image()
        print(img_data)
        found = [d for d in img_data if target == d['class']]
        if len(found) == 0:
            return camera_pos
        box = found[0]['box']
        print(f'box: {box}')
        target_images.append({'box': box, 'img': np.copy(current_img)})
        target_pos = box[1] + (box[3] - box[1]) / 2 - 0.5
        print(f'relative pos: {target_pos}')
        if abs(target_pos) < 0.02:
            return camera_pos
        deg = np.rad2deg(np.arcsin(target_pos))
        if deg < 0:
            direction = set_counterclockwise()
            step_delta = 1
        else:
            direction = set_clockwise()
            step_delta = -1

        # center lidar
        steps_necessary = int(abs(deg) * 360 / steps)
        for i in range(steps_necessary):
            step(delay2, 0)
            camera_pos += step_delta


def scan_image():
    camera.capture(raw_capture, 'bgr')
    a = raw_capture.array
    global current_img
    current_img = a

    shp = ', '.join([str(d) for d in a.shape])
    files = {'file': a.tobytes(), 'shape': shp}

    r = requests.post(url, files=files)
    d = json.loads(r.text)['response']
    raw_capture.truncate(0)
    return d


def main():
    mode = 2
    d1 = 0.01
    turn_stepper_on()
    drive_setup()
    calibrate(d1, 0, mode)
    points = turn(mode, d1)
    a = np.array(points)
    for i,tar in enumerate(target_images):
        plot_target(tar['box'], tar['img'], f'target{i}.png')
    np.save('target.npy', a)
    


if __name__ == "__main__":
    main()
