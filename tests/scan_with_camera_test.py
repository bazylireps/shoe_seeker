import sys
from time import sleep
from lidar.stepper_utils import step, calibrate, set_stepper_mode, is_on_spot, change_direction, set_clockwise, \
    set_counterclockwise, turn_stepper_on
from lidar.lidar_utils import get_distance
import numpy as np

from picamera.array import PiRGBArray
from picamera import PiCamera
import requests
import json

url = 'http://127.0.0.1:5000'

IM_WIDTH = 1280
IM_HEIGHT = 720

camera = PiCamera()
camera.resolution = (IM_WIDTH, IM_HEIGHT)
raw_capture = PiRGBArray(camera, size=(IM_WIDTH, IM_HEIGHT))

delay2 = 0.003

target = 'bicycle'

def turn(mode, delay1):
    found_object = False
    missed_steppes_scan = 0
    missed_steppes_return = 0
    set_stepper_mode(mode)
    direction = set_counterclockwise()

    steps = 200
    steps *= mode

    sleep(0.1)
    print("skanuje w trybie 1/%d" % (mode))
    sleep(0.1)
    points = list()
    for x in range(steps):
        step(delay1, 0)
        dist, strength = get_distance()
        points.append((x, dist, strength))
        if x % (25 * mode) == 0:
            img_data = scan_image()
            print(img_data)
            found = [d for d in img_data if target == d['class']]
            if len(found) > 0:
                found_object = True
                steps = x
                break
    if not is_on_spot() and not found_object:
        missed_steppes_scan = calibrate(delay1, delay2, mode)
        print("zgubiono %d krokow podczas skanu!" % missed_steppes_scan)
    change_direction(direction)
    for x in range(steps):
        step(delay2, 0)

    sleep(0.5)
    if not is_on_spot():
        missed_steppes_return = calibrate(delay1, delay2, mode)
        print("zgubiono %d krokow podczas powrotu!" % (missed_steppes_return))

    return points, missed_steppes_scan, missed_steppes_return


def scan_image():
    camera.capture(raw_capture, 'bgr')
    a = raw_capture.array

    shp = ', '.join([str(d) for d in a.shape])
    files = {'file': a.tobytes(), 'shape': shp}

    r = requests.post(url, files=files)
    d = json.loads(r.text)['response']
    raw_capture.truncate(0)
    return d


def main():
    mode = 2
    d1 = 0.01
    turn_stepper_on()
    calibrate(d1, 0, mode)
    points = turn(mode, d1)
    a = np.array(points)
    # np.save('scan.npy', a)


if __name__ == "__main__":
    main()
