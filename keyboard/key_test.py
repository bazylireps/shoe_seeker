import curses
from curses import wrapper
import RPi.GPIO as g
from time import sleep

from drive.motor_utils import drive_setup, stop, forward, backward, left, right

def main(stdscr):
    state = 'hold'
    drive_setup()
    stdscr.nodelay(True)
    i = 0
    while True:
        sleep(0.01)
        
        k = stdscr.getch()
        if k == curses.KEY_UP:
            i = 0
            if state != 'fwd':
                forward()
            state = 'fwd'
        elif k == curses.KEY_DOWN:
            i = 0
            if state != 'bwd':
                backward()
            state = 'bwd'
        elif k == curses.KEY_LEFT:
            i = 0
            if state != 'left':
                left()
            state = 'letf'
        elif k == curses.KEY_RIGHT:
            i = 0
            if state != 'right':
                right()
            state = 'right'
        else:
            i += 1
            if i > 10:
                state = 'hold'
                stop()

try:
    wrapper(main)
except KeyboardInterrupt:
    stop()
    print('interrupted')
finally:
    stop()
    g.cleanup()

