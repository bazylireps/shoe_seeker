from time import sleep
import RPi.GPIO as g
import sys
import pigpio

right_forward = 23
right_backward = 24
left_forward = 7
left_backward = 25
pwm_pin = 18

pi = pigpio.pi()

dc = 0.7

d1 = 0.07
d2 = 0.2

pwm = None


def drive_setup():
    g.setmode(g.BCM)
    g.setwarnings(False)
    g.setup(left_forward, g.OUT)
    g.setup(left_backward, g.OUT)
    g.setup(right_forward, g.OUT)
    g.setup(right_backward, g.OUT)
    g.setup(pwm_pin, g.OUT)

    pi.set_PWM_dutycycle(pwm_pin, int(256 * dc))
    pi.set_PWM_frequency(pwm_pin, 20)


def stop():
    g.output(right_forward, 0)
    g.output(left_forward, 0)
    g.output(right_backward, 0)
    g.output(left_backward, 0)


def forward():
    g.output(right_forward, 1)
    g.output(left_forward, 1)


def backward():
    g.output(right_backward, 1)
    g.output(left_backward, 1)


def left():
    g.output(right_forward, 1)
    g.output(left_backward, 1)


def right():
    g.output(right_backward, 1)
    g.output(left_forward, 1)


def step_forward():
    forward()
    sleep(d1)
    stop()
    sleep(d2)


def step_backwards():
    backward()
    sleep(d1)
    stop()
    sleep(d2)


def step_left():
    left()
    sleep(d1)
    stop()
    sleep(d2)


def step_right():
    right()
    sleep(d1)
    stop()
    sleep(d2)


if __name__ == '__main__':
    try:
        '''
        t = float(sys.argv[1])
        t2 = float(sys.argv[2])
        '''
        r = int(sys.argv[1])

        drive_setup()
        sleep(1)
        '''
        for i in range(r):
            print('forward')
            forward()
            sleep(t)
            stop()
            sleep(t2)

        for i in range(r):
            print('backward')
            backward()
            sleep(t)
            stop()
            sleep(t2)
        '''
        for i in range(r):
            print('left')
            step_left()
        '''
        for i in range(r):
            print('right')
            step_right()
        '''
        stop()
    except Exception as e:
        print(e)
        stop()
    finally:
        print('done')
        g.cleanup()
        # pwm.stop()
