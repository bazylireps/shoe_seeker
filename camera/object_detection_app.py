from flask import Flask, request
import numpy as np
from camera.detector import Detector

app = Flask(__name__)

detector = Detector()


@app.route('/', methods=['GET','POST'])
def detect():
    print('got request')
    file = request.files['file'].read()
    shp = [int(s) for s in request.files['shape'].read().decode('utf-8').split(',')]
    print(shp)
    a = np.frombuffer(file, dtype=np.uint8).reshape(*shp)
    print('created array')
    res = detector.detect(a)
    return {'response': res}


app.run()
