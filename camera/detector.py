import os
import cv2
import numpy as np
import tensorflow as tf
from utils import label_map_util


class Detector:

    def __init__(self):
        # Set up camera constants
        IM_WIDTH = 1280
        IM_HEIGHT = 720

        # Name of the directory containing the object detection module we're using
        MODEL_NAME = 'ssdlite_mobilenet_v2_coco_2018_05_09'

        # Grab path to current working directory
        CWD_PATH = os.getcwd()

        # Path to frozen detection graph .pb file, which contains the model that is used
        # for object detection.
        PATH_TO_CKPT = os.path.join(CWD_PATH, 'models', MODEL_NAME, 'frozen_inference_graph.pb')

        # Path to label map file
        PATH_TO_LABELS = os.path.join(CWD_PATH, 'models', 'mscoco_label_map.pbtxt')

        # Number of classes the object detector can identify
        NUM_CLASSES = 90

        label_map = label_map_util.load_labelmap(PATH_TO_LABELS)

        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                                    use_display_name=True)

        self.category_index = label_map_util.create_category_index(categories)

        # Load the Tensorflow model into memory.
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            print('create od_graph_def')
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
                print('loading serialized graph')
                serialized_graph = fid.read()
                print('parse string graph')
                od_graph_def.ParseFromString(serialized_graph)
                print('import_graph_def')
                tf.import_graph_def(od_graph_def, name='')

            print('creating session')
            self.sess = tf.Session(graph=detection_graph)

        # Define input and output tensors (i.e. data) for the object detection classifier

        # Input tensor is the image
        self.image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

        # Output tensors are the detection boxes, scores, and classes
        # Each box represents a part of the image where a particular object was detected
        self.detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

        # Each score represents level of confidence for each of the objects.
        # The score is shown on the result image, together with the class label.
        self.detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
        self.detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

        # Number of objects detected
        self.num_detections = detection_graph.get_tensor_by_name('num_detections:0')

        # Initialize frame rate calculation
        self.frame_rate_calc = 1
        self.freq = cv2.getTickFrequency()
        self.font = cv2.FONT_HERSHEY_SIMPLEX

    def detect(self, frame):
        print('start detect')
        frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame_expanded = np.expand_dims(frame_rgb, axis=0)

        # Perform the actual detection by running the model with the image as input
        (boxes, scores, classes, num) = self.sess.run(
            [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
            feed_dict={self.image_tensor: frame_expanded})
        scores = np.squeeze(scores)
        classes = np.squeeze(classes).astype(np.int32)
        boxes = np.squeeze(boxes)
        num = np.squeeze(num)
        classes = [{'class': self.category_index[c]['name'], 'score': float(scores[i]), 'box': boxes[i].tolist()}
                   for i, c in enumerate(classes) if scores[i] > 0.5]
        
        print('end detect')
        return classes
