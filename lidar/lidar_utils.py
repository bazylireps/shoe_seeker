import sys
import serial

from time import sleep

ser = serial.Serial('/dev/serial0', 115200, timeout=1)
ser.write(bytes(b'B'))
ser.write(bytes(b'W'))
ser.write(bytes(2))
ser.write(bytes(0))
ser.write(bytes(0))
ser.write(bytes(0))
ser.write(bytes(0))
ser.write(bytes(26))
if not ser.is_open:
    ser.open()


def get_distance():
    # if ser.is_open == False:
    #    ser.open()

    distance = 0
    strength = 0
    ser.reset_input_buffer()
    running = True
    count = ser.in_waiting
    while (count < 8):
        count = ser.in_waiting
    recv = ser.read(9)
    ser.reset_input_buffer()
    if recv[0] == 0x59 and recv[1] == 0x59:
        distance = recv[2] + recv[3] * 256
        strength = recv[4] + recv[5] * 256
        # ser.reset_input_buffer()
    if not ser:
       ser.close()
    return [distance, strength]


if __name__ == '__main__':
    for x in range(10000):
        sys.stdout.write('\r{}'.format(get_distance()[0] / 10))
        sys.stdout.flush()
        sleep(0.1)
    print()
