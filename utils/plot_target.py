import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import json


def plot_target(box, img, fname='target.png'):
    target_pos = box[1] + (box[3] - box[1]) / 2 - 0.5
    deg = np.rad2deg(np.arcsin(target_pos))

    if deg < 0:
        direction = 'left'
    else:
        direction = 'right'

    box[0] = int(box[0] * img.shape[0])
    box[2] = int(box[2] * img.shape[0])
    box[1] = int(box[1] * img.shape[1])
    box[3] = int(box[3] * img.shape[1])

    tp_displ = int(box[1] + (box[3] - box[1]) / 2)

    a1 = patches.Arrow(tp_displ, box[0], 0, (box[2] - box[0]) / 2, width=8, color='white')
    a2 = patches.Arrow(tp_displ, box[2], 0, -(box[2] - box[0]) / 2, width=8, color='white')
    a3 = patches.Arrow(box[1], box[0] + (box[2] - box[0]) / 2, (box[3] - box[1]) / 2, 0, width=8, color='white')
    a4 = patches.Arrow(box[3], box[0] + (box[2] - box[0]) / 2, -(box[3] - box[1]) / 2, 0, width=8, color='white')

    p1 = (box[1], box[0])
    p2 = (box[3], box[2])
    rect = patches.Rectangle(p1, box[3] - box[1], box[2] - box[0], linewidth=1, edgecolor='white', facecolor='none')

    fig, ax = plt.subplots(figsize=(10, 10))
    ax.imshow(img)
    ax.set_xticks(np.linspace(0, img.shape[1], 21))
    ax.set_xticklabels(np.linspace(0, 1, 21).round(decimals=2), rotation=45, ha="right")
    ax.set_xlabel('x')

    ax.set_yticks(np.linspace(0, img.shape[0], 11))
    ax.set_yticklabels(np.linspace(1, 0, 11).round(decimals=2), rotation=45, ha="right")
    ax.set_ylabel('y')

    ax.add_patch(rect)
    ax.add_patch(a1)
    ax.add_patch(a2)
    ax.add_patch(a3)
    ax.add_patch(a4)

    ax.annotate(f'target off by {deg:.2f}deg {direction}', (box[3], box[0]))
    pth = f'/home/pi/shoe_seeker/{fname}'
    plt.savefig(pth, format='png')
    plt.close()
